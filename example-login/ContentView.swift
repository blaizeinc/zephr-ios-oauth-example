//
//  ContentView.swift
//  example-login
//
//  Created by Ben Kennedy on 24/02/2021.
//

import SwiftUI
import BetterSafariView
import Blaize

struct ContentView: View {
    let session = URLSession.shared
    let callbackURLScheme = "com.zephr.example-login"

    @ObservedObject var userSettings = UserSettings()
    @ObservedObject var authConfig = AuthConfig()

    @State private var startingWebAuthenticationSession = false
    @State private var completePartialRegistration = false
    @State var firstName: String = ""

    var body: some View {
        NavigationView {
            if completePartialRegistration {
                    Form {
                        Section(header: Text("Complete Registration")) {
                            TextField("First Name", text: $firstName)
                            Button(action: {
                                BlaizePublic.client.register(token: authConfig.token!, attributes: ["first-name": firstName]) { result in
                                    switch result {
                                    case .success(let sessionId):
                                        setUserSessionId(sessionId: sessionId)
                                    case .userAlreadyExists:
                                        print("Error: user already exists")
                                    case .emailDomainBlacklisted:
                                        print("Error: email domain blacklisted")
                                    case .badRequest:
                                        print("Error: bad registration request")
                                    }
                                }
                            }) {
                                Text("Submit")
                            }
                        }
                    }
                    .navigationBarTitle("Register")
                
            } else if userSettings.sessionId.isEmpty {
                VStack {
                    Text("You are currently signed out.")
                        .padding()

                    Button(action: {
                        startOAuthFlow(provider: AuthProvider.google) { locationUrl in
                            triggerWebAuthenticationSession(provider: AuthProvider.google, url: locationUrl)
                        }
                    }) {
                        Text("Sign in with Google")
                    }
                    .buttonStyle(ZephrButtonStyle())
                    
                    Button(action: {
                        startOAuthFlow(provider: AuthProvider.facebook) { locationUrl in
                            triggerWebAuthenticationSession(provider: AuthProvider.facebook, url: locationUrl)
                        }
                    }) {
                        Text("Sign in with Facebook")
                    }
                    .buttonStyle(ZephrButtonStyle())
                    
                    Button(action: {
                        startOAuthFlow(provider: AuthProvider.apple) { locationUrl in
                            triggerWebAuthenticationSession(provider: AuthProvider.facebook, url: locationUrl)
                        }
                    }) {
                        Text("Sign in with Apple")
                    }
                    .buttonStyle(ZephrButtonStyle())
                }
                .navigationBarTitle("Sign in")
                .webAuthenticationSession(isPresented: $startingWebAuthenticationSession) {
                    WebAuthenticationSession(
                        url: authConfig.url!,
                        callbackURLScheme: callbackURLScheme
                    ) { callbackURL, error in
                        guard let callbackUrl = callbackURL else {
                            print("Callback error - auth flow cancelled")
                            return closeWebAuthenticationSession()
                        }

                        print (callbackUrl)

                        if (error != nil) {
                            print("API error")
                            return closeWebAuthenticationSession()
                        }

                        guard let callbackComponents = callbackUrl.components else {
                            print("Callback components invalid")
                            return closeWebAuthenticationSession()
                        }

                        let queryItems = callbackComponents.queryItems ?? []
                        print(queryItems)

                        switch queryItems["status"] {
                        case "success":
                            print("Login successful")
                            setUserSessionId(sessionId: queryItems["session_id"]!)
                        case "partial":
                            print("Partial registration successful")
                            triggerPartialRegistration(token: queryItems["state_key"]!)
                        case "failure":
                            print("Auth response error")
                            print(queryItems["message"] ?? "")
                        default:
                            print("Unknown auth response status")
                            print (callbackURL!)
                        }
                    }
                }

            } else {
                VStack {
                    Text("You are signed in.")
                    Text(userSettings.sessionId)
                        .padding()

                    Button(action: {
                        BlaizePublic.client.logout(sessionId: userSettings.sessionId) { result in
                            switch result {
                            case .success:
                                print("Logged out")
                            case .failure:
                                print("Something went wrong")
                            }
                            setUserSessionId(sessionId: "")
                        }
                    }) {
                        Text("Sign out")
                    }
                    .buttonStyle(ZephrButtonStyle())
                }
                .navigationBarTitle("Hello there")
            }
        }
    }
    
    func setUserSessionId(sessionId: String) {
        DispatchQueue.main.async {
            self.completePartialRegistration = false
            self.userSettings.sessionId = sessionId
        }
    }
    
    func triggerPartialRegistration(token: String) {
        DispatchQueue.main.async {
            self.completePartialRegistration = true
            self.authConfig.token = token
        }
    }
    
    func triggerWebAuthenticationSession(provider: AuthProvider, url: URL?) {
        DispatchQueue.main.async {
            self.authConfig.url = url
            self.authConfig.provider = provider
            self.startingWebAuthenticationSession = true
        }
    }
    
    func closeWebAuthenticationSession() {
        DispatchQueue.main.async {
            self.startingWebAuthenticationSession = false
        }
    }

    func startOAuthFlow(provider: AuthProvider, callback: @escaping (URL?) -> ()) {
        BlaizePublic.client.startOAuthFlow(provider: provider) { result in
            switch result {
            case .success(let locationUrl):
                callback(locationUrl)
            case .failure:
                print("Could not start OAuth flow")
                return
            }
        }
    }
    
}

class AuthConfig: ObservableObject {
    @Published var url: URL?
    @Published var provider: AuthProvider?
    @Published var token: String?
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct ZephrButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        return configuration.label
            .padding()
            .background(Color.pink)
            .foregroundColor(Color.white)
            .cornerRadius(9)
            .opacity(configuration.isPressed ? 0.8 : 1)
            .scaleEffect(configuration.isPressed ? 0.9 : 1)
            .animation(.easeInOut(duration: 0.1))
    }
}
