//
//  UserSettings.swift
//  example-login
//
//  Created by Ben Kennedy on 01/03/2021.
//

import Foundation
import Combine

class UserSettings: ObservableObject {
    @Published var sessionId: String {
        didSet {
            UserDefaults.standard.set(sessionId, forKey: "sessionId")
        }
    }
    
    init() {
        self.sessionId = UserDefaults.standard.object(forKey: "sessionId") as? String ?? ""
    }
}
