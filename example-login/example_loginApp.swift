//
//  example_loginApp.swift
//  example-login
//
//  Created by Ben Kennedy on 24/02/2021.
//

import SwiftUI
import Blaize

@main
struct example_loginApp: App {
    init() {
        BlaizeConfiguration.shared.overridePublicBaseUrl = URL(string: "https://YOUR_CDN_URL_HERE")!
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
